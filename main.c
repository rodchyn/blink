#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <signal.h>
#include <linux/kd.h>
#include <sys/ioctl.h>

int execute;

void trap(int signal)
{ 
  execute = 0;
  //printf("Caught signal %d\n", signal); 
}

int main()
{
  signal(SIGINT, &trap);
  execute = 1;
  int fd = open("/dev/console", O_NOCTTY);
  while(execute) {
    ioctl(fd, KDSETLED, 0);// выключить все
    usleep(200000);
    ioctl(fd, KDSETLED, 4 );
    usleep(200000);
    //ioctl(fd, KDSETLED,6); //NumLock==2,ScrollLock==1,CapsLock==4. Комбинируя сумму этих чисел можем вкючить те или инные диоды. В данном случае будут гореть только Num и Caps
  }
  signal(SIGINT, SIG_DFL);
  ioctl(fd, KDSETLED, 0 );
  close(fd);
  return 0;
}
